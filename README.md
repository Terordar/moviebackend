# movie-backend

## Getting Started
Clone the repo:
```
git clone git@gitlab.com:Terordar/moviebackend.git
cd moviebackend
```

Install dependencies:
```
npm i
```

Set environment (vars):
```
cp .env.example .env
```

Start server:
```
npm start
```
