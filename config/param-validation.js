const { Joi } = require('celebrate');

const movieBody = Joi.object().keys({
  title: Joi.string().required(),
  director: Joi.string().required(),
  genre: Joi.string().required(),
  description: Joi.string().required(),
  releaseDate: Joi.date().timestamp().required()
});

module.exports = {
  // POST /api/movies
  createMovie: {
    body: movieBody
  },
  updateMovie: {
    body: movieBody,
    params: {
      movieId: Joi.string().hex().required()
    }
  }
};
