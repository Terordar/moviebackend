const mongoose = require('mongoose');

const app = require('./config/express');

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
}).catch((err) => {
  console.error(err);
  console.info('MongoDB connection error. Please make sure MongoDB is running.');
  process.exit();
});


app.listen(process.env.PORT || 8080, () => {
  console.info(`Server started on port ${process.env.PORT} (${process.env.ENV})`);
});
