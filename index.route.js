const router = require('express').Router();

const movieRoutes = require('./server/movie/movie.route');

router.get('/health-check', (req, res) => res.send('OK'));

router.use('/movies', movieRoutes);

module.exports = router;
