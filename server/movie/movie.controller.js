const httpStatus = require('http-status');
const mongoose = require('mongoose');

const Movie = require('./movie.model');
const APIError = require('../../server/helpers/APIError');

const APIErrorOnUpdateOrCreateMovie = err => {
  let status = httpStatus.UNPROCESSABLE_ENTITY;

  if (err.errors && err.errors.title && err.errors.title.kind === 'unique') {
    status = httpStatus.CONFLICT;
  }

  return new APIError(err.message, status, true);
};

exports.load = async (req, res, next, id) => {
  try {
    req.movie = await Movie.get(id);
    return next();
  } catch (err) {
    // Mongoose will throw an error if id isn't a mongo ObjectID
    if (err instanceof mongoose.CastError) {
      return next(new APIError('No such movie exists!', httpStatus.NOT_FOUND));
    }

    return next(err);
  }
};

exports.delete = async (req, res, next) => {
  try {
    await req.movie.remove();
    return res.sendStatus(httpStatus.NO_CONTENT);
  } catch (err) {
    return next(err);
  }
};

exports.get = (req, res) => res.json(req.movie);

exports.list = async (req, res, next) => {
  const { limit = 50, skip = 0 } = req.query;

  try {
    const nbMovies = await Movie.estimatedDocumentCount();
    const data = await Movie.list({ limit, skip });
    const pages = Math.ceil(nbMovies / limit);

    return await res.json({
      pages,
      data
    });
  } catch (err) {
    return next(err);
  }
};

exports.create = async (req, res, next) => {
  const {
    title,
    director,
    releaseDate,
    description,
    genre
  } = req.body;

  try {
    const createdMovie = await Movie.create({
      title,
      director,
      releaseDate,
      description,
      genre
    });

    return await res.json(createdMovie);
  } catch (err) {
    return next(APIErrorOnUpdateOrCreateMovie(err));
  }
};

exports.update = async (req, res, next) => {
  const movie = req.movie;
  const {
    title,
    director,
    releaseDate,
    description,
    genre
  } = req.body;

  movie.title = title;
  movie.director = director;
  movie.releaseDate = releaseDate;
  movie.description = description;
  movie.genre = genre;

  try {
    const updatedMovie = await movie.save();
    return await res.json(updatedMovie);
  } catch (err) {
    return next(APIErrorOnUpdateOrCreateMovie(err));
  }
};
