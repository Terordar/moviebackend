const mongoose = require('mongoose');
const httpStatus = require('http-status');
const uniqueValidator = require('mongoose-unique-validator');
const APIError = require('../helpers/APIError');

const movieSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  releaseDate: {
    type: Date,
    required: true
  },
  director: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  genre: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

movieSchema.statics = {
  async get(id) {
    const movie = await this.findById(id).exec();

    if (movie) {
      return movie;
    }

    const err = new APIError('No such movie exists!', httpStatus.NOT_FOUND);
    return Promise.reject(err);
  },
  async list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .exec();
  }
};


movieSchema.index({
  title: 1,
  director: 1,
}, {
  unique: true,
});

movieSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Movie', movieSchema);
