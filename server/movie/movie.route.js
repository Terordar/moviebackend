const router = require('express').Router();
const { celebrate } = require('celebrate');
const paramValidation = require('../../config/param-validation');
const movieController = require('./movie.controller');

// abortEarly to false so that all validation errors are sent
router.route('/')
  .get(movieController.list)
  .post(celebrate(paramValidation.createMovie, { abortEarly: false }), movieController.create);

// Fetch movie for every request with movieId param
router.param('movieId', movieController.load);

router.route('/:movieId')
  .get(movieController.get)
  .put(celebrate(paramValidation.updateMovie, { abortEarly: false }), movieController.update)
  .delete(movieController.delete);

module.exports = router;
